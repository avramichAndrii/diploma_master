﻿import React, { Component } from 'react';
import './AuthorizationForm.css'

export class AuthorizationForm extends Component {
    static displayName = AuthorizationForm.name;

    constructor(props) {
        super(props);
        this.state = { currentCount: 0 };
        this.incrementCounter = this.incrementCounter.bind(this);
    }

    incrementCounter() {
        this.setState({
            currentCount: this.state.currentCount + 1
        });
    }

    render() {
        return (
            <div class="login">
                <h1>Login</h1>
                <form method="post">
                    <input type="text" name="u" placeholder="Username" required="required" />
                    <input type="password" name="p" placeholder="Password" required="required" />
                    <button type="submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
                </form>
            </div>
        );
    }
}