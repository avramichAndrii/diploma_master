﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using User_DAL;
using User_DAL.Interfaces;
using User_DAL.Models;

namespace User_controllers.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userService;

        public UserController(IUserRepository repo)
        {
            _userService = repo;
        }

        /// <summary>
        /// Get all menues
        /// </summary>
        /// <returns>List of menues with dishes and menues information</returns>
        [HttpGet]
        public async Task<ActionResult<IReadOnlyCollection<User>>> GetAllAsync()
        {
            try
            {
                IReadOnlyCollection<User> userModels = await _userService.GetAllUsers();
                return new OkObjectResult(userModels);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get menu by menue id
        /// </summary>
        /// <param name="id">Unique menu id</param>
        /// <returns>Menu with information and dishes</returns>
        [HttpGet("{userId}")]
        public async Task<ActionResult<User>> GetAsync(Guid userId)
        {
            try
            {
                User res = await _userService.GetUserById(userId);
                return res;
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Create new menu
        /// </summary>
        /// <param name="request">Menue information from UI</param>
        /// <returns>Menu, which was created</returns>
        //[HttpPost]
        //public async Task<ActionResult<User>> CreateUserAsync([FromBody] MenuRequest request)
        //{
        //    try
        //    {
        //        var model = request.MapFrom();
        //        var res = await _userService.Create(model);
        //        return res.MapTo();
        //    }
        //    catch (MenuDomainException ex)
        //    {
        //        return BadRequest(new { message = ex.Message });
        //    }
        //}


        /// <summary>
        /// Update existing menu
        /// </summary>
        /// <param name="menuId"> Unique menu id</param>
        /// <param name="request"> Uodating information from UI</param>
        /// <returns>Menu, which was updated</returns>
        //[HttpPut("{menuId}")]
        //public async Task<ActionResult<MenuResponse>> UpdateMenuAsync(Guid menuId, [FromBody] MenuRequest request)
        //{
        //    try
        //    {
        //        User model = request.MapFrom();
        //        User res = await _userService.Update(menuId, model);
        //        return res.MapTo();
        //    }
        //    catch (MenuDomainException ex)
        //    {
        //        return BadRequest(new { message = ex.Message });
        //    }
        //}

        /// <summary>
        /// Delete menu with dishes by menu id
        /// </summary>
        /// <param name="menuId">Unique menu id</param>
        /// <returns>Action status</returns>
        //[HttpDelete("{menuId}")]
        //public async Task<ActionResult> DeleteAsync(Guid menuId)
        //{
        //    try
        //    {
        //        await _userService.DeleteAsync(menuId);
        //        return Ok();
        //    }
        //    catch (MenuDomainException ex)
        //    {
        //        return BadRequest(new { message = ex.Message });
        //    }
        //}
    }
}
