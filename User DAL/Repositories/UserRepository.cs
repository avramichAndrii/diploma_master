﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User_DAL.Interfaces;
using User_DAL.Models;

namespace User_DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        public readonly UserDbContext _context;

        public UserRepository(UserDbContext context) 
        {
            _context = context;
        }

        public async Task AddUser(User user)
        {
            _context.UserModels.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUser(User user)
        {
            _context.UserModels.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetUserById(Guid userId)
        {
            return await _context.UserModels.FirstOrDefaultAsync(x => x.Id == userId);
        }

        public async Task UpdateUser(User user)
        {
            _context.UserModels.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<User>> GetAllUsers()
        {
            return await _context.UserModels.ToListAsync();
        }
    }
}
