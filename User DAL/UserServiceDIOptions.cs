﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User_DAL.Interfaces;
using User_DAL.Repositories;

namespace User_DAL
{
    public static class UserServiceDIOptions
    {
        //private const string DBConnectionString = nameof(DBConnectionString);
        public static void RegisterUserPersistence(this IServiceCollection collection, IConfiguration configuration)
        {
            //var connection = configuration.GetConnectionString(DBConnectionString);
            var connection = "InMemoryTest";
            collection.AddDbContextPool<UserDbContext>(builder => builder.UseInMemoryDatabase(connection));
            collection.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
