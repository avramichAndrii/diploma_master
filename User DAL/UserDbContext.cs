﻿using System;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using User_DAL.Models;

namespace User_DAL
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions options) : base(options) 
        { 
        }

        public DbSet<User> UserModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<User>(new UserMapping());
            base.OnModelCreating(modelBuilder);
        }

    }
}
