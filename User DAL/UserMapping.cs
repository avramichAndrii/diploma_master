﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using User_DAL.Models;

namespace User_DAL
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        private const string TableName = "User";
        private const int MaxSize = 1500;
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(t => t.Id);
            builder.Property(x => x.FirstName);
            builder.Property(x => x.LastName);
            builder.Property(x => x.Email);
            builder.Property(x => x.Created);
            builder.Property(x => x.Roles);
            builder.Property(x => x.Password);
        }
    }
}
