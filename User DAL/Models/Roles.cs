﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User_DAL.Models
{
    public enum Role
    {
        Admin = 0,
        Manager = 1,
        User = 2,
        PostUser = 3
    }
}
