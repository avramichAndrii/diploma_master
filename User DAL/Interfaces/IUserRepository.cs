﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User_DAL.Models;

namespace User_DAL.Interfaces
{
    public interface IUserRepository
    {
        public Task AddUser(User user);

        public Task UpdateUser(User user);

        public Task DeleteUser(User user);

        public Task<User> GetUserById(Guid userId);

        public Task<IReadOnlyCollection<User>> GetAllUsers();
    }
}
